# Projet gamma_software_with_angular_back

Partie back du projet avec Angular.

Pour cette partie, j'ai fait le choix de dialoguer via une API entre Symfony et Angular.

Pour cela, j'utilise ApiPlatform.

## Context du projet

Projet consistant à importer un fichier Excel contenant des groupes de musique.

Un fichier exemple se trouve [ici](./private/test.xlsx).

Le projet contient :
- [x] Un CRUD via l'API pour gérer les groupes de musique
- [ ] Une route API pour importer un fchier XSLX
- [x] Une commande permettant d'importer le fichier de test en base de données

## Non inclus dans le projet

Voici les fonctionnalités non demandées pour le projet qu'il serait judicieux de rajouter :

- Un système d'authentification (JWT) avec une gestion des utilisateurs
- La vérification que le groupe de musique n'aie pas déjà été ajouté avant de l'insérer en base de données