<?php

namespace App\Utils;

use PhpOffice\PhpSpreadsheet\IOFactory;

class XlsxReader
{
    /**
     * Transforms a spreadsheet to array of data
     *
     * @param string $filePath
     * @return array
     */
    public function readFile(string $filePath): array {

        // Create the reader
        $reader = IOFactory::createReaderForFile($filePath);
        $reader->setReadDataOnly(true);
        $spreatsheet = $reader->load($filePath);

        // Get the first sheet
        $sheet = $spreatsheet->getSheet($spreatsheet->getFirstSheetIndex());

        // Tranform sheet data to array
        $arrayData = $sheet->toArray();

        $data = [];
        $headers = $arrayData[0];

        // Remove headers from array
        array_shift($arrayData);

        // Associate each cell to own header
        foreach ($arrayData as $row) {
            $tab = [];

            foreach ($row as $index => $cell) {
                $tab[$headers[$index]] = $cell;
            }

            $data[] = $tab;
        }

        return $data;
    }
}