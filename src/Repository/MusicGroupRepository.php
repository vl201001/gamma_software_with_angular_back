<?php

namespace App\Repository;

use App\Entity\MusicGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MusicGroup>
 *
 * @method MusicGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method MusicGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method MusicGroup[]    findAll()
 * @method MusicGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MusicGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MusicGroup::class);
    }

//    /**
//     * @return MusicGroup[] Returns an array of MusicGroup objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?MusicGroup
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    public function save(MusicGroup $mg, bool $flush = false) {
        $this->_em->persist($mg);

        if ($flush) {
            $this->flush();
        }
    }

    public function remove(MusicGroup $mg, bool $flush = false) {
        $this->_em->remove($mg);

        if ($flush) {
            $this->flush();
        }
    }

    public function flush() {
        $this->_em->flush();
    }
}
