<?php

namespace App\Factory;

use App\Entity\MusicGroup;
use DateTimeImmutable;

class MusicGroupFactory
{
    /**
     * Transforms array from spreadsheet to MusicGroupValidator entity
     *
     * @param array $data
     * @return MusicGroup
     */
    public static function createFromArray(array $data): MusicGroup
    {
        $mg = (new MusicGroup())
            ->setName($data['Nom du groupe'])
            ->setOrigin($data['Origine'])
            ->setCity($data['Ville'])
            ->setStartedAt(self::createDateFromStringOfYear($data['Année début']))
            ->setEndedAt(self::createDateFromStringOfYear($data['Année séparation']))
            ->setFounders($data['Fondateurs'])
            ->setNbMembers($data['Membres'])
            ->setMusicType($data['Courant musical'])
            ->setDescription($data['Présentation'])
        ;

        return $mg;
    }

    private static function createDateFromStringOfYear(?string $year): ?DateTimeImmutable
    {
        if (!is_null($year) && !empty($year)) {
            return DateTimeImmutable::createFromFormat('d-m-Y', '01-01-' . $year);
        }

        return null;
    }
}