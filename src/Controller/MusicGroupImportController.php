<?php

namespace App\Controller;

use App\Entity\MusicGroup;
use App\Repository\MusicGroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MusicGroupImportController extends AbstractController
{
    public function __construct(private MusicGroupRepository $repository) {

    }

    public function __invoke(Request $request): MusicGroup
    {
        dump($request);
        return new MusicGroup();
    }
}