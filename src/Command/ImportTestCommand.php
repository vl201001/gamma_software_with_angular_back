<?php

namespace App\Command;

use App\Entity\MusicGroup;
use App\Factory\MusicGroupFactory;
use App\Repository\MusicGroupRepository;
use App\Utils\XlsxReader;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(
    name: 'app:import:test',
    description: 'Tries to import the file located in the private directory',
)]
class ImportTestCommand extends Command
{
    public function __construct(
        private KernelInterface $kernel,
        private XlsxReader $xlsxReader,
        private MusicGroupRepository $musicGroupRepository
    )
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $filePath = $this->kernel->getProjectDir() . '/private/test.xlsx';

        $fileData = $this->xlsxReader->readFile($filePath);

        $this->insertInDatabase($fileData);

        $io->success('Import successfull');

        return Command::SUCCESS;
    }

    /**
     * Inserts all data in database
     *
     * @param array $data
     * @return void
     */
    private function insertInDatabase(array $data) {
        foreach ($data as $item) {
            $this->musicGroupRepository->save(MusicGroupFactory::createFromArray($item));
        }

        $this->musicGroupRepository->flush();
    }
}